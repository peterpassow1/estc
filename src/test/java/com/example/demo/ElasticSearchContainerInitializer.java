package com.example.demo;


import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.elasticsearch.ElasticsearchContainer;

import java.io.IOException;

public class ElasticSearchContainerInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private static final String ELASTICSEARCH_USERNAME = "test";
    private static final String ELASTICSEARCH_PASSWORD = "elastic";

    @java.lang.Override
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
        try {
            ElasticsearchContainer container = new ElasticsearchContainer("docker.elastic.co/elasticsearch/elasticsearch:7.10.1");
            container.start();

            final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            credentialsProvider.setCredentials(AuthScope.ANY,
                    new UsernamePasswordCredentials(ELASTICSEARCH_USERNAME, ELASTICSEARCH_PASSWORD));

            RestClient client = RestClient.builder(HttpHost.create(container.getHttpHostAddress()))
                    .setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider))
                    .build();

            Response response = client.performRequest(new Request("GET", "/_cluster/health"));

            TestPropertyValues.of("elasticsearch.uri=" + container.getHttpHostAddress())
                    .applyTo(configurableApplicationContext.getEnvironment());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
