package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;

import org.apache.http.HttpHost;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.io.IOException;

@SpringBootTest
@ContextConfiguration(initializers = ElasticSearchContainerInitializer.class)
class DemoApplicationTests {

    @Value("${elasticsearch.uri}")
    private String url;

    @Test
    void contextLoads() throws IOException {
        RestClient client = RestClient.builder(HttpHost.create(url)).build();

        Response response = client.performRequest(new Request("GET", "/_cluster/health"));

        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(200);
    }
}
